package com.easyfoot.WsEasyFoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class WsEasyFootApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsEasyFootApplication.class, args);
	}
}
