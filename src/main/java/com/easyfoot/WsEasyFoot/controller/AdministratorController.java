package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Administrator;
import com.easyfoot.WsEasyFoot.model.Place;
import com.easyfoot.WsEasyFoot.repository.AdministratorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdministratorController {

    @Autowired
    AdministratorRepository administratorRepository;

    @PostMapping("/login")
    public Administrator authLogin(@RequestParam("login")String login, @RequestParam("password")String password){

        Administrator admin = administratorRepository.findByLogin(login);
        if(admin != null){
            if(admin.getPassword().equals(password)){
                return admin;
            }
        }
        throw new ResourceNotFoundException("Login","Admin",admin);
    }

    // Get all admins
    @GetMapping("/getAll")
    public List<Administrator> getAllAdmins() {
        return administratorRepository.findAll();
    }

    // Create new admin
    @PostMapping("/create")
    public Administrator createAdmin(@Valid @RequestBody Administrator admin) {
        return administratorRepository.save(admin);
    }

    // Get a admin
    @GetMapping("/get/{id}")
    public Administrator getAdminById(@PathVariable(value = "id") Long adminId) {
        return administratorRepository.findById(adminId).orElseThrow(() -> new ResourceNotFoundException("Administrator", "id", adminId));
    }

    // Update admin
    @PutMapping("/updateAdmin/{id}")
    public Administrator updateAdmin(@PathVariable(value = "id") Long adminId, @Valid @RequestBody Administrator admin) {

        Administrator oldAdmin = administratorRepository.findById(adminId).orElseThrow(() -> new ResourceNotFoundException("Administrator", "id", adminId));

        oldAdmin.setLogin(admin.getLogin());
        oldAdmin.setPassword(admin.getPassword());

        Administrator newAdmin = administratorRepository.save(oldAdmin);
        return newAdmin;
    }

    // delete admin
    @DeleteMapping("/deleteAdmin/{id}")
    public ResponseEntity<?> deleteAdmin(@PathVariable(value = "id") Long adminId) {
        Administrator admin = administratorRepository.findById(adminId).orElseThrow(() -> new ResourceNotFoundException("Administrator", "id", adminId));

        administratorRepository.delete(admin);

        return ResponseEntity.ok().build();
    }


}
