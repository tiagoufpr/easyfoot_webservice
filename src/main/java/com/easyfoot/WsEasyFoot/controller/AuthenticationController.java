package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    PlayerRepository playerRepository;

    @PostMapping("/login")
    public Player authLogin(@RequestParam("email")String email,@RequestParam("password")String password){

        Player player = playerRepository.findByEmail(email);
        if(player != null){
            if(player.getPassword().equals(password)){
                return player;
            }
        }
    throw new ResourceNotFoundException("Login","Player",player);
    }
    
    @PostMapping("/loginsocial")
    public Player authLoginSocial(@RequestParam("email")String email){

        Player player = playerRepository.findByEmail(email);
        if(player != null){
                return player;
        }
        
    throw new ResourceNotFoundException("Login","Player",player);
    }
}
