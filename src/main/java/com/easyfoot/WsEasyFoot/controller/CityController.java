package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.City;
import com.easyfoot.WsEasyFoot.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/city")
public class CityController {

    @Autowired
    CityRepository cityRepository;

    // Get all cities
    @GetMapping("/getAll")
    public List<City> getAllCities() {
        return cityRepository.findAll();
    }

    // Create new city
    @PostMapping("/create")
    public City createCity(@Valid @RequestBody City city) {
        return cityRepository.save(city);
    }

    // Get a city
    @GetMapping("/get/{id}")
    public City getCityById(@PathVariable(value = "id") Long cityId) {
        return cityRepository.findById(cityId).orElseThrow(() -> new ResourceNotFoundException("City", "id", cityId));
    }

    // Atualizar local
    @PutMapping("/updateCity/{id}")
    public City updateCity(@PathVariable(value = "id") Long cityId, @Valid @RequestBody City city) {

        City oldCity = cityRepository.findById(cityId).orElseThrow(() -> new ResourceNotFoundException("City", "id", cityId));

        oldCity.setName(city.getName());
        oldCity.setState(city.getState());

        City newCity = cityRepository.save(oldCity);
        return newCity;
    }

    // Deletar cidade
    @DeleteMapping("/deleteCity/{id}")
    public ResponseEntity<?> deleteCity(@PathVariable(value = "id") Long cityId) {
        City city = cityRepository.findById(cityId).orElseThrow(() -> new ResourceNotFoundException("City", "id", cityId));

        cityRepository.delete(city);

        return ResponseEntity.ok().build();
    }
    
    
    
}
