/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Conversation;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.repository.ConversationRepository;

import com.easyfoot.WsEasyFoot.repository.PlayerRepository;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiago
 */

@RestController
@CrossOrigin
@RequestMapping("/conversation")
public class ConversationController {
    @Autowired
    PlayerRepository playerRepository;
    
    @Autowired
    ConversationRepository conversationRepository;
    
    @PostMapping("/create")
    public Conversation createConversation(@Valid @RequestBody Conversation conversation) {
        return conversationRepository.save(conversation);
    }
    
    // Get Players tha have Conversations
    @GetMapping("/getPlayers/{id}")
    public List<Player> getById(@PathVariable(value = "id") Long idSender) {
        Player playerLogged = playerRepository.findById(idSender).orElseThrow(() -> new ResourceNotFoundException("Player", "id", idSender));
        List<Conversation> conversations = conversationRepository.findByPlayerSenderOrPlayerReceiver(playerLogged, playerLogged);
        
        List<Player> players = new ArrayList<>();;
        
        for(Conversation conversation : conversations){
            if(!players.contains(conversation.getPlayerReceiver()))
                players.add(conversation.getPlayerReceiver());
            if(!players.contains(conversation.getPlayerSender()))
                players.add(conversation.getPlayerSender());
        }
        
        players.remove(playerLogged);
        
        return players;
    }
    
    // Get Conversation
    @GetMapping("/get/{idsender}&{idreceiver}")
    public List<Conversation> getConversationById(@PathVariable(value = "idsender") Long idSender, @PathVariable("idreceiver") Long idReceiver) {
        Player playerLogged = playerRepository.findById(idSender).orElseThrow(() -> new ResourceNotFoundException("Player", "id", idSender));
        Player playerOther = playerRepository.findById(idReceiver).orElseThrow(() -> new ResourceNotFoundException("Player", "id", idReceiver));
        
        return conversationRepository.findByPlayerSenderAndPlayerReceiverOrPlayerSenderAndPlayerReceiver(playerLogged, playerOther, playerOther, playerLogged);
    }
}
