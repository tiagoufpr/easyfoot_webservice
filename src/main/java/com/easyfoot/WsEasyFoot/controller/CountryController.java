package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Country;
import com.easyfoot.WsEasyFoot.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryRepository countryRepository;
    
    // Get all countrys
    @GetMapping("/getAll")
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    // Create new country
    @PostMapping("/create")
    public Country createCountry(@Valid @RequestBody Country country) {
        return countryRepository.save(country);
    }

    // Get a country
    @GetMapping("/get/{id}")
    public Country getCountryById(@PathVariable(value = "id") Long countryId) {
        return countryRepository.findById(countryId).orElseThrow(() -> new ResourceNotFoundException("Country", "id", countryId));
    }

    // Update country
    @PutMapping("/updateCountry/{id}")
    public Country updateCountry(@PathVariable(value = "id") Long countryId, @Valid @RequestBody Country country) {

        Country oldCountry = countryRepository.findById(countryId).orElseThrow(() -> new ResourceNotFoundException("Country", "id", countryId));
        oldCountry.setName(country.getName());
        Country newCountry = countryRepository.save(oldCountry);

        return newCountry;
    }

    // Delete country
    @DeleteMapping("/deleteCountry/{id}")
    public ResponseEntity<?> deleteCountry(@PathVariable(value = "id") Long countryId) {
        Country country = countryRepository.findById(countryId).orElseThrow(() -> new ResourceNotFoundException("Country", "id", countryId));

        countryRepository.delete(country);

        return ResponseEntity.ok().build();
    }

}
