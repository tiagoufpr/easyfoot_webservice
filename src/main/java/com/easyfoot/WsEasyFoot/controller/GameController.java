package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.AlreadyExistsException;
import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.model.Team;
import com.easyfoot.WsEasyFoot.repository.GameRepository;
import com.easyfoot.WsEasyFoot.repository.PlayerRepository;
import com.easyfoot.WsEasyFoot.repository.RegistrationRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/game")
public class GameController {

    @Autowired
    GameRepository gameRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;
    
    @Autowired
    RegistrationRepository registrationRepository;

    // Get all games
    @GetMapping("/getAll")
    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    // Create game
    @PostMapping("/create")
    public Game createGame(@Valid @RequestBody Game game) {
        if(!gameRepository.existsByName(game.getName()))
            return gameRepository.save(game);
        else
            throw new AlreadyExistsException("name","Game",game.getName());
    }

    // Get one game
    @GetMapping("/get/{id}")
    public Game getGameById(@PathVariable(value = "id") Long gameId) {
        return gameRepository.findById(gameId).orElseThrow(() -> new ResourceNotFoundException("Game", "id", gameId));
    }

    // Update game
    @PutMapping("/update/{id}")
    public Game updateGame(@PathVariable(value = "id") Long gameId, @Valid @RequestBody Game gameDetails) {

        Game game = gameRepository.findById(gameId).orElseThrow(() -> new ResourceNotFoundException("Game", "id", gameId));

        //if(gameRepository.existsByName(gameDetails.getName()))
        //    throw new AlreadyExistsException("name","update Game", gameDetails.getName());
        //verificar o nome se ja existe e se o id nao eh o mesmo !

        game.setName(gameDetails.getName());
        game.setPlace(gameDetails.getPlace());
        game.setDateTime(gameDetails.getDateTime());
        game.setCreatedBy(gameDetails.getCreatedBy());
        game.setTeams(gameDetails.getTeams());
        game.setConfirmed(gameDetails.getConfirmed());
        game.setFinished(gameDetails.getFinished());

        Game updatedGame = gameRepository.save(game);
        return updatedGame;
    }

    // Delete game
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteGame(@PathVariable(value = "id") Long gameId) {
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new ResourceNotFoundException("Game", "id", gameId));

        Registration registration = registrationRepository.findByGame(game);
        
        registrationRepository.delete(registration);
        
        gameRepository.delete(game);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/get/byPlayer/{id}")
    public List<Game> findGamesByPlayer(@PathVariable(value = "id") Long playerId){

        Player p = playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));

        //Set<Game> gamesSet = new HashSet<>();
        //List<Game> games = new ArrayList<>();
        
        //games = gameRepository.findGamesByTeams(p.getTeams());

        //gamesAux.add(games);
        
        //p.getTeams().forEach();
        
        //Game game = new Game();
        
        //game = gameRepository.findGamesByTeam(.);

        //teams.forEach(team -> gamesSet.addAll(team.getGames()));

        //games.addAll(gamesSet);
        
        //teamRepository.findAllByPlayers(p)
        
        List<Team> teams = new ArrayList<>();
        
        teams = p.getTeams();

        return gameRepository.findGamesByTeamsIn(teams);
    }

    @GetMapping("/get/findGamesByPlayerAndConfirmed/{id}&{confirmed}")
    public List<Game> findGamesByPlayerAndConfirmed(@PathVariable(value = "id") Long playerId, @PathVariable("confirmed") Boolean confirmed){
        Player p = playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));

        
        if(confirmed == true){
           return gameRepository.findGamesByTeamsInAndConfirmedIsTrueAndFinishedIsFalse(p.getTeams());
        }else if(confirmed == false){
            return gameRepository.findGamesByTeamsInAndConfirmedIsFalseAndFinishedIsFalse(p.getTeams());
        } else
            return gameRepository.findGamesByTeamsInAndConfirmedIsNull(p.getTeams());
    }    
}
