package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Place;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import com.easyfoot.WsEasyFoot.repository.PlaceRepository;
import com.easyfoot.WsEasyFoot.repository.RegistrationRepository;

@RestController
@CrossOrigin
@RequestMapping("/place")
public class PlaceController {
    @Autowired
    PlaceRepository placeRepository;
    
    @Autowired
    RegistrationRepository registrationRepository;
    
    @Autowired
    GameRepository gameRepository;

    // Get all places
    @GetMapping("/getAll")
    public List<Place> getAllPlaces() {
        return placeRepository.findAll();
    }

    // Create new place
    @PostMapping("/create")
    public Place createPlace(@Valid @RequestBody Place place) {
        return placeRepository.save(place);
    }

    // Get a place
    @GetMapping("/get/{id}")
    public Place getPlaceById(@PathVariable(value = "id") Long placeId) {
        return placeRepository.findById(placeId).orElseThrow(() -> new ResourceNotFoundException("Place", "id", placeId));
    }

    // Atualizar local
    @PutMapping("/updatePlace/{id}")
    public Place updatePlace(@PathVariable(value = "id") Long placeId, @Valid @RequestBody Place place) {

       Place oldPlace = placeRepository.findById(placeId).orElseThrow(() -> new ResourceNotFoundException("Place", "id", placeId));

       oldPlace.setName(place.getName());
       oldPlace.setCity(place.getCity());
       oldPlace.setLatitude(place.getLatitude());
       oldPlace.setLongitude(place.getLongitude());

        Place newPlace = placeRepository.save(oldPlace);
        return newPlace;
    }

    // Deletar cidade
    @DeleteMapping("/deletePlace/{id}")
    public ResponseEntity<?> deletePlace(@PathVariable(value = "id") Long placeId) {
        Place place = placeRepository.findById(placeId).orElseThrow(() -> new ResourceNotFoundException("Place", "id", placeId));

        List<Game> games = gameRepository.findGamesByPlace(place);
        
        Registration registration;
        
        for (int i = 0; i < games.size(); i++) {
            registration = registrationRepository.findByGame(games.get(i));
            
            if(registration != null)
                registrationRepository.delete(registration);
            
            gameRepository.delete(games.get(i));
	}   
        
        placeRepository.delete(place);

        return ResponseEntity.ok().build();
    }

}
