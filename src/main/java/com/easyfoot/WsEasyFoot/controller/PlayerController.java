package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.AlreadyExistsException;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.repository.PlaceRepository;
import com.easyfoot.WsEasyFoot.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.City;
import com.easyfoot.WsEasyFoot.model.Conversation;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.model.Team;
import com.easyfoot.WsEasyFoot.model.TeamRanking;
import com.easyfoot.WsEasyFoot.repository.ConversationRepository;
import com.easyfoot.WsEasyFoot.repository.GameRepository;
import com.easyfoot.WsEasyFoot.repository.RegistrationRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRankingRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRepository;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/player")
public class PlayerController {

	@Autowired
	PlayerRepository playerRepository;
        
        @Autowired
        RegistrationRepository registrationRepository;
    
        @Autowired
        GameRepository gameRepository;
        
        @Autowired
        TeamRankingRepository teamRankingRepository;
        
        @Autowired
        TeamRepository teamRepository;
        
        @Autowired
        ConversationRepository conversationRepository;
	
	// Get all players
	@GetMapping("/getAll")
	public List<Player> getAllPlayers() {
	    return playerRepository.findAll();
	}
	
	// Create player
	@PostMapping("/create")
	public Player createPlayer(@Valid @RequestBody Player player) {
	    if(!playerRepository.existsByEmail(player.getEmail()))
	        return playerRepository.save(player);
	    else
	        throw new AlreadyExistsException("email","Player",player.getEmail());
	}
	
	// Pegar um jogador por id
	@GetMapping("/get/{id}")
	public Player getPlayerById(@PathVariable(value = "id") Long playerId) {
	    return playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));
	}
        
        // Pegar varios jogador por nome
	@GetMapping("/getbyname/{name}")
	public List<Player> findByNameContainingIgnoreCase(@PathVariable(value = "name") String name) {
	    return playerRepository.findByNameContainingIgnoreCase(name);
	}
        
        // Pegar um jogador por email
	@GetMapping("/getbyemail/{email}")
	public Player getPlayerByEmail(@PathVariable(value = "email") String email) {
	    return playerRepository.findByEmail(email);
	}
        
        // Pegar varios jogador por email
	@GetMapping("/getbyemails/{email}")
	public List<Player> getPlayerByEmails(@PathVariable(value = "email") String email) {
	    return playerRepository.findByEmailContainingIgnoreCase(email);
	}
        
        // Pegar um jogador por cidade
	@GetMapping("/getbycity/{city}")
	public Player getPlayerByCity(@PathVariable(value = "city") String city) {
            City citySearch = null;
	    return playerRepository.findByCity(citySearch);
	}
	
	// Atualizar jogador
	@PutMapping("/update/{id}")
	public Player updatePlayer(@PathVariable(value = "id") Long playerId, @Valid @RequestBody Player playerDetails) {

		Player player = playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));

	    //if(playerRepository.existsByEmail(playerDetails.getEmail())) throw new AlreadyExistsException("email","update Player", playerDetails.getEmail());
		//teste
	    player.setName(playerDetails.getName());
	    player.setEmail(playerDetails.getEmail());
	    player.setPassword(playerDetails.getPassword());
	    player.setCity(playerDetails.getCity());
	    player.setTeams(playerDetails.getTeams());
	    player.setImageUrl(playerDetails.getImageUrl());

	    Player updatedPlayer = playerRepository.save(player);
	    return updatedPlayer;
	}
	
	// Excluir jogador
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deletePlayer(@PathVariable(value = "id") Long playerId) {
		Player player = playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Jogador", "id", playerId));
                Registration registration;
                TeamRanking teamRanking;
                List<Team> teamsAux;
                List<Game> games;
                List<Conversation> conversations;
                
		try {
                    List<Team> teams = teamRepository.findAllByPlayers(player);
                    
                    for (int z = 0; z < teams.size(); z++) {
                    
                        teamRanking = teamRankingRepository.findByTeam(teams.get(z));

                        if(teamRanking != null)
                            teamRankingRepository.delete(teamRanking);

                        teamsAux = new ArrayList();
                        teamsAux.add(teams.get(z));

                        games = gameRepository.findGamesByTeamsIn(teamsAux);

                        for (int i = 0; i < games.size(); i++) {
                            registration = registrationRepository.findByGame(games.get(i));

                            if(registration != null)
                                registrationRepository.delete(registration);

                            gameRepository.delete(games.get(i));
                        }   
                        
                        teamRepository.delete(teams.get(z));
                    }
                    
                    
                    conversations = conversationRepository.findByPlayerSenderOrPlayerReceiver(player, player);
                    
                    for (int x = 0; x < conversations.size(); x++) {
                        conversationRepository.delete(conversations.get(x));
                    }
                    
                    playerRepository.delete(player);
		}catch(Exception e){
			e.printStackTrace();
		}
	    return ResponseEntity.ok().build();
	}	
}
