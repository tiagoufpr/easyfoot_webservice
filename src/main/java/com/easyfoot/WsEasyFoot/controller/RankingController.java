package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.PlayerRanking;
import com.easyfoot.WsEasyFoot.model.Team;
import com.easyfoot.WsEasyFoot.model.TeamRanking;
import com.easyfoot.WsEasyFoot.repository.PlayerRankingRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRankingRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import org.springframework.data.domain.Sort;

@RestController
@CrossOrigin
@RequestMapping("/ranking")
public class RankingController {

    @Autowired
    TeamRankingRepository teamRankingRepository;
    
    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerRankingRepository playerRankingRepository;

    //get All Team Rankings !
    @GetMapping("/team/getAll")
    public List<TeamRanking> getAllTeamRankings(){
        return teamRankingRepository.findAll((Sort.by(Sort.Direction.DESC, "points")));
    }

    //create Team Ranking
    @PostMapping("/team/create")
    public TeamRanking createTeamRanking(@Valid @RequestBody TeamRanking teamRanking) {
        return teamRankingRepository.save(teamRanking);
    }

    // get one team Ranking
    @GetMapping("/team/get/{id}")
    public TeamRanking getTeamRankingById(@PathVariable(value = "id") Long teamRankingId) {
        Team team = teamRepository.findById(teamRankingId).orElseThrow(() -> new ResourceNotFoundException("Team", "id", teamRankingId));
        TeamRanking t = teamRankingRepository.findByTeam(team);
        return t;
    }

    // update teamRanking
    @PutMapping("/team/update/{id}")
    public TeamRanking updateTeamRanking(@PathVariable(value = "id") Long teamRankingId, @Valid @RequestBody TeamRanking teamRankingDetails) {

        TeamRanking teamRanking = teamRankingRepository.findById(teamRankingId).orElseThrow(() -> new ResourceNotFoundException("TeamRanking", "id", teamRankingId));

        teamRanking.setPoints(teamRankingDetails.getPoints());
        teamRanking.setTeam(teamRankingDetails.getTeam());

        TeamRanking updatedTeamRanking = teamRankingRepository.save(teamRanking);
        return updatedTeamRanking;
    }

    // delete teamRanking
    @DeleteMapping("/team/delete/{id}")
    public ResponseEntity<?> deleteTeamRanking(@PathVariable(value = "id") Long teamRankingId) {
        TeamRanking teamRanking = teamRankingRepository.findById(teamRankingId).orElseThrow(() -> new ResourceNotFoundException("TeamRanking", "id", teamRankingId));

        teamRankingRepository.delete(teamRanking);

        return ResponseEntity.ok().build();
    }


    //get All Player Rankings !
    @GetMapping("/player/getAll")
    public List<PlayerRanking> getAllPlayerRankings(){
        return playerRankingRepository.findAll();
    }

    //create Team Ranking
    @PostMapping("/player/create")
    public PlayerRanking createPlayerRanking(@Valid @RequestBody PlayerRanking playerRanking) {
        return playerRankingRepository.save(playerRanking);
    }

    // get one team Ranking
    @GetMapping("/player/get/{id}")
    public PlayerRanking getPlayerRankingById(@PathVariable(value = "id") Long playerRankingId) {
        return playerRankingRepository.findById(playerRankingId).orElseThrow(() -> new ResourceNotFoundException("PlayerRanking", "id", playerRankingId));
    }

    // update teamRanking
    @PutMapping("/player/update/{id}")
    public PlayerRanking updatePlayerRanking(@PathVariable(value = "id") Long playerRankingId, @Valid @RequestBody PlayerRanking playerRankingDetails) {

        PlayerRanking playerRanking = playerRankingRepository.findById(playerRankingId).orElseThrow(() -> new ResourceNotFoundException("PlayerRanking", "id", playerRankingId));

        playerRanking.setPoints(playerRankingDetails.getPoints());
        playerRanking.setPlayer(playerRankingDetails.getPlayer());

        PlayerRanking updatedPlayerRanking = playerRankingRepository.save(playerRanking);
        return updatedPlayerRanking;
    }

    // delete teamRanking
    @DeleteMapping("/player/delete/{id}")
    public ResponseEntity<?> deletePlayerRanking(@PathVariable(value = "id") Long playerRankingId) {
        PlayerRanking playerRanking = playerRankingRepository.findById(playerRankingId).orElseThrow(() -> new ResourceNotFoundException("PlayerRanking", "id", playerRankingId));

        playerRankingRepository.delete(playerRanking);

        return ResponseEntity.ok().build();
    }


}
