package com.easyfoot.WsEasyFoot.controller;


import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.repository.GameRepository;
import com.easyfoot.WsEasyFoot.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    RegistrationRepository registrationRepository;
    
    @Autowired
    GameRepository gameRepository;

    // Get all registrations
    @GetMapping("/getAll")
    public List<Registration> getAllRegistrations() {
        return registrationRepository.findAll();
    }

    // Create registration
    @PostMapping("/create")
    public Registration createRegistration(@Valid @RequestBody Registration registration) {
            return registrationRepository.save(registration);
            //a principio nao vou travar o registro, depois tenho que ver se tem alguma regra de negocio
    }

    // Get one Registration
    @GetMapping("/get/{id}")
    public Registration getRegistrationById(@PathVariable(value = "id") Long registrationId) {
        return registrationRepository.findById(registrationId).orElseThrow(() -> new ResourceNotFoundException("Registration", "id", registrationId));
    }

    // Atualizar jogador
    @PutMapping("/update/{id}")
    public Registration updateRegistration(@PathVariable(value = "id") Long registrationId, @Valid @RequestBody Registration registrationDetails) {

        Registration registration = registrationRepository.findById(registrationId).orElseThrow(() -> new ResourceNotFoundException("Registration", "id", registrationId));

        registration.setGame(registrationDetails.getGame());
        registration.setPlayer(registrationDetails.getPlayer());
        registration.setTeam(registrationDetails.getTeam());

        Registration updatedRegistration = registrationRepository.save(registration);
        return updatedRegistration;
    }

    // Excluir cadastro
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRegistration(@PathVariable(value = "id") Long registrationId) {
        Registration registration = registrationRepository.findById(registrationId).orElseThrow(() -> new ResourceNotFoundException("Registration", "id", registrationId));

        registrationRepository.delete(registration);

        return ResponseEntity.ok().build();
    }
    
    @DeleteMapping("/deleteandgame/{id}")
    public ResponseEntity<?> deleteRegistrationAndGame(@PathVariable(value = "id") Long gameId) {
        Registration registration = registrationRepository.findRegistrationByGameId(gameId);

        if(registration != null)
            registrationRepository.delete(registration);

        Game game = gameRepository.findById(gameId).orElseThrow(() -> new ResourceNotFoundException("Game", "id", gameId));
        
        if(game != null)
            gameRepository.delete(game);
        
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get/byGame/{id}")
    public Registration getRegistrationByGameId(@PathVariable(value = "id")Long gameId){
        Registration registration = registrationRepository.findRegistrationByGameId(gameId);
        return registration;
    }

}
