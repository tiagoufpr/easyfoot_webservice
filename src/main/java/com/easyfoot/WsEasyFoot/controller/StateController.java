package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.State;
import com.easyfoot.WsEasyFoot.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/state")
public class StateController {

    @Autowired
    StateRepository stateRepository;

    // Get all states
    @GetMapping("/getAll")
    public List<State> getAllStates() {
        return stateRepository.findAll();
    }

    // Create new state
    @PostMapping("/create")
    public State createState(@Valid @RequestBody State state) {
        return stateRepository.save(state);
    }

    // Get a state
    @GetMapping("/get/{id}")
    public State getStateById(@PathVariable(value = "id") Long stateId) {
        return stateRepository.findById(stateId).orElseThrow(() -> new ResourceNotFoundException("State", "id", stateId));
    }

    // Update state
    @PutMapping("/updateState/{id}")
    public State updateState(@PathVariable(value = "id") Long stateId, @Valid @RequestBody State state) {

        State oldState = stateRepository.findById(stateId).orElseThrow(() -> new ResourceNotFoundException("State", "id", stateId));
        oldState.setName(state.getName());
        State newState = stateRepository.save(oldState);

        return newState;
    }

    // Delete state
    @DeleteMapping("/deleteState/{id}")
    public ResponseEntity<?> deleteState(@PathVariable(value = "id") Long stateId) {
        State state = stateRepository.findById(stateId).orElseThrow(() -> new ResourceNotFoundException("State", "id", stateId));

        stateRepository.delete(state);

        return ResponseEntity.ok().build();
    }
    
    

}
