package com.easyfoot.WsEasyFoot.controller;

import com.easyfoot.WsEasyFoot.exception.AlreadyExistsException;
import com.easyfoot.WsEasyFoot.exception.ResourceNotFoundException;
import com.easyfoot.WsEasyFoot.model.City;
import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.model.Team;
import com.easyfoot.WsEasyFoot.model.TeamRanking;
import com.easyfoot.WsEasyFoot.repository.CityRepository;
import com.easyfoot.WsEasyFoot.repository.GameRepository;
import com.easyfoot.WsEasyFoot.repository.PlayerRepository;
import com.easyfoot.WsEasyFoot.repository.RegistrationRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRankingRepository;
import com.easyfoot.WsEasyFoot.repository.TeamRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/team")
public class TeamController {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerRepository playerRepository;
    
    @Autowired
    TeamRankingRepository teamRankingRepository;
    
    @Autowired
    RegistrationRepository registrationRepository;
    
    @Autowired
    GameRepository gameRepository;
    
    @Autowired
    CityRepository cityRepository;

    //get All Teams !
    @GetMapping("/getAll")
    public List<Team>getAllTeams(){
        return teamRepository.findAll();
    }

    //create Team
    // Create team
    @PostMapping("/create")
    public Team createTeam(@Valid @RequestBody Team team) {
        if(!teamRepository.existsByName(team.getName()))
            return teamRepository.save(team);
        else
            throw new AlreadyExistsException("name","Team",team.getName());
    }

    // get one team
    @GetMapping("/get/{id}")
    public Team getTeamById(@PathVariable(value = "id") Long teamId) {
        return teamRepository.findById(teamId).orElseThrow(() -> new ResourceNotFoundException("Team", "id", teamId));
    }
    
    
    // Pegar um time por nome
    @GetMapping("/getbyname/{name}")
    public List<Team> getPlayerByName(@PathVariable(value = "name") String name) {
	return teamRepository.findByNameContainingIgnoreCase(name);
    }
    
    
    // Pegar um time por cidade
    @GetMapping("/getbycity/{city}")
    public List<Team> getPlayerByCity(@PathVariable(value = "city") String city) {
        City citySearch = cityRepository.findByNameContainingIgnoreCase(city);
	return teamRepository.findByCity(citySearch);
    }
       

    // update team
    @PutMapping("/update/{id}")
    public Team updateTeam(@PathVariable(value = "id") Long teamId, @Valid @RequestBody Team teamDetails) {

        Team team = teamRepository.findById(teamId).orElseThrow(() -> new ResourceNotFoundException("Team", "id", teamId));

        //if(!team.getName().equals(teamDetails.getName())  && (teamRepository.existsByName(teamDetails.getName())))
        //    throw new AlreadyExistsException("email","update Team", teamDetails.getName());

        team.setName(teamDetails.getName());
        team.setCaptain(teamDetails.getCaptain());
        team.setCoCaptain(teamDetails.getCoCaptain());
        team.setPlayers(teamDetails.getPlayers());
        //team.setGames(teamDetails.getGames());
        team.setCity(teamDetails.getCity());
        Team updatedTeam = teamRepository.save(team);
        return updatedTeam;
    }

    // delete team
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTeam(@PathVariable(value = "id") Long teamId) {
        Team team = teamRepository.findById(teamId).orElseThrow(() -> new ResourceNotFoundException("Team", "id", teamId));
        
        TeamRanking teamRanking = teamRankingRepository.findByTeam(team);

        if(teamRanking != null)
            teamRankingRepository.delete(teamRanking);     
        
        List<Team> teams = new ArrayList();
        
        teams.add(team);
        
        List<Game> games = gameRepository.findGamesByTeamsIn(teams);
        
        Registration registration;
        
        for (int i = 0; i < games.size(); i++) {
            registration = registrationRepository.findByGame(games.get(i));
            
            if(registration != null)
                registrationRepository.delete(registration);
            
            gameRepository.delete(games.get(i));
	}   

        teamRepository.delete(team);
        
        return ResponseEntity.ok().build();
    }

    //get all teams by player
    @GetMapping("/get/byPlayer/{id}")
    public List<Team>findTeamsByPlayer(@PathVariable(value = "id")Long playerId){

        Player p = playerRepository.findById(playerId).orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));

        List<Team> teams = teamRepository.findAllByPlayers(p);

        return teams;

    }

}
