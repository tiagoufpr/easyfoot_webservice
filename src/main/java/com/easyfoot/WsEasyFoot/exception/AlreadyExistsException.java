package com.easyfoot.WsEasyFoot.exception;

public class AlreadyExistsException extends RuntimeException {

    private String field;
    private String resourceName;
    private String resourceValue;
    public AlreadyExistsException(String field, String resourceName,String resourceValue) {
        super(String.format("%s exists in %s; value= '%s'", field, resourceName,resourceValue));
        this.field = field;
        this.resourceName = resourceName;
        this.resourceValue=resourceValue;
    }
}
