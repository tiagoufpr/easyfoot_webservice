package com.easyfoot.WsEasyFoot.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="player")
@EntityListeners(AuditingEntityListener.class)
//ainda nao coloquei o campo foto
public class Player implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//auto-increment
	private Long id;
	
	@NotBlank
	private String name;
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String password;

	@ManyToOne
	private City city;

	/*@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(
			name = "player_teams",
			joinColumns = { @JoinColumn(name = "player_id") },
			inverseJoinColumns = { @JoinColumn(name = "teams_id") }
	)*/
	@ManyToMany(mappedBy = "players")
	@JsonIgnore
	private List<Team> teams = new ArrayList<>();

	private String imageUrl;

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
//teste commit na branch develop
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
