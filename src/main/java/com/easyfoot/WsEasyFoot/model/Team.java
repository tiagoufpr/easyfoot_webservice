package com.easyfoot.WsEasyFoot.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="team")
@EntityListeners(AuditingEntityListener.class)
public class Team implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @NotBlank
    private String name;

    @ManyToOne
    private City city;

    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "team_players",
            joinColumns = { @JoinColumn(name = "teams_id") },
            inverseJoinColumns = { @JoinColumn(name = "players_id") }
    )

    private Set<Player> players = new HashSet<>();

    //@ManyToMany(cascade = { CascadeType.ALL })
    //@JoinTable(
    //        name = "team_games",
    //        joinColumns = { @JoinColumn(name = "teams_id") },
    //        inverseJoinColumns = { @JoinColumn(name = "games_id") }
    //)

    //private Set<Game> games = new HashSet<>();

    @ManyToOne
    private Player captain = new Player();

    @ManyToOne
    private Player coCaptain = new Player();

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Player getCaptain() {
        return captain;
    }

    public void setCaptain(Player captain) {
        this.captain = captain;
    }

    public Player getCoCaptain() {
        return coCaptain;
    }

    public void setCoCaptain(Player coCaptain) {
        this.coCaptain = coCaptain;
    }

    //public Set<Game> getGames() {
    //    return games;
    //}

    //public void setGames(Set<Game> games) {
    //    this.games = games;
   // }
}