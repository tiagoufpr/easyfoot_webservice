package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator,Long> {

    Administrator findByLogin(String login);

}
