package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository  extends JpaRepository<City,Long> {

    public City findByNameContainingIgnoreCase(String city);
}
