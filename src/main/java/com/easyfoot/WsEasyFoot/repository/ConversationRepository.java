/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Conversation;
import com.easyfoot.WsEasyFoot.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation,Player> {

    List<Conversation> findByPlayerSenderAndPlayerReceiverOrPlayerSenderAndPlayerReceiver(Player playerA, Player playerB, Player playerC, Player playerD);
    
    List<Conversation> findByPlayerSenderOrPlayerReceiver(Player playerA, Player playerB);

}