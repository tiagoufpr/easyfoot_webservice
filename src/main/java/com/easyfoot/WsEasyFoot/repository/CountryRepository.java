package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country,Long> {
}
