package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Place;
import com.easyfoot.WsEasyFoot.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface GameRepository extends JpaRepository<Game,Long> {

    Boolean existsByName(String name);
    
    List<Game> findGamesByTeamsInAndConfirmedIsTrueAndFinishedIsFalse(List<Team> teams);
    
    List<Game> findGamesByTeamsInAndConfirmedIsFalseAndFinishedIsFalse(List<Team> teams);

    List<Game> findGamesByTeamsInAndConfirmedIsTrue(List<Team> teams);

    List<Game> findGamesByTeamsInAndConfirmedIsFalse(List<Team> teams);

    List<Game> findGamesByTeamsInAndConfirmedIsNull(List<Team> teams);

    List<Game> findGamesByTeamsIn(List<Team> teams);

    public List<Game> findGamesByPlace(Place place);
    
}
