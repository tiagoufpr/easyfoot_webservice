package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Long> {
}
