package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.PlayerRanking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRankingRepository extends JpaRepository<PlayerRanking,Long> {
}
