package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.City;
import com.easyfoot.WsEasyFoot.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.easyfoot.WsEasyFoot.model.Player;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{

    Player findByName(String name);
    List<Player> findByNameContainingIgnoreCase(String name);
    
    Player findByEmail(String email);
    List<Player> findByEmailContainingIgnoreCase(String email);
    
    Player findByCity(City city);

    Boolean existsByEmail(String email);
}
