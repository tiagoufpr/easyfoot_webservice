package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Game;
import com.easyfoot.WsEasyFoot.model.Registration;
import com.easyfoot.WsEasyFoot.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrationRepository extends JpaRepository<Registration,Long> {

    List<Registration> findAllByPlayerId(Long playerId);

    Registration findRegistrationByGameId(Long gameId);

    public Registration findByGame(Game game);
}
