package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.State;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepository extends JpaRepository<State,Long> {
}
