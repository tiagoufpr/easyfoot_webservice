package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.Team;
import com.easyfoot.WsEasyFoot.model.TeamRanking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRankingRepository extends JpaRepository<TeamRanking,Long> {
    TeamRanking findByTeam(Team team);
}
