package com.easyfoot.WsEasyFoot.repository;

import com.easyfoot.WsEasyFoot.model.City;
import com.easyfoot.WsEasyFoot.model.Player;
import com.easyfoot.WsEasyFoot.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamRepository extends JpaRepository<Team,Long> {

   Boolean existsByName(String name);
   
   Team findByName(String name);
   List<Team> findByNameContainingIgnoreCase(String name);
   
   List<Team> findByCity(City city);

   List<Team> findAllByPlayers(Player player);
}
